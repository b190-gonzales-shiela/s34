// npm init
/*
	- triggering this command will prompt the user for different settings that will define the application
	- using this command will make a "package.json" in our repo
	- package.json tracks the version of our application, depending on the settings we have set. also, we can see the dependencies that we have installed inside of this file
*/

/*
	npm install express
		-after triggering this command, express will now be listed as a "dependency". this can be seen inside the "package.json" file under the "dependencies" property

		- installing any dependency using npm will result into creating "node_modules" folder and package-lock.json
			"node_modules" directory should be left on the local repository because some of the hosting sites will fail 
			to load the repository once it found the "node_modules" inside the repo. another reasone where node_modules is left on the local repository is it takes too much time to commit
			"node_modules" is also where the dependencies needed files are stored.

		".gitignore" files, as the name suggests will tell the git what files are to be spared/"ignored" in terms of commiting and pushing.

		-"npm install" this command is used when there are available dependencies inside out "package.json" but are not yet installed
		inside the repo/project
*/

// we need now the express module since in this dependency, it has already built in codes that will allow the dev
    // dev to create a server in a breeze/easier

const express= require("express");

// express()- allows us to create our server using express
// simply put, the app varaiable in our server

const app= express();

// setting up port variable

const port=3000;

// app.use lets the server to handle data from requests
// "app.use(express.json());" allows the app to read and handle json data type
// methods use from express middlewares

	// middleware- software that provide common services and capabilities for the application

app.use(express.json());
// app.use(express.urlencoded({extended:true})); allows the server to read data from forms
// by default, info received from the url  can only be received as a string or an array
// but using extended:true for the argument allows us to receive information from other data types 
	// such as objects which will ruse throughout our application
app.use(express.urlencoded({extended:true}));

// SECTION_ ROUTES

// get method
/*

	-express has methods corresponding to each HTTP methods
	-the route below expects to receive a GET at the base URI "/"
*/
app.get("/",(req,res)=>{
	res.send("Hello World");
})

/*------			MINI ACTIVITY		-------*/
app.get("/hello",(req,res)=>{
	res.send("Hello from/ hello endpoint");
})

// POST METHOD
// this route expects to receive a post at request uri

app.post("/hello",(req,res)=>{

	// res.bpdy contains the contents/data of the request body
	res.send(`hello there ${req.body.firstName} ${req.body.lastName}!`);
});
app.post("/signup",(req,res)=>{

if (req.body.firstName!=="" && req.body.lastName!==""){
	users.push(req.body);
	res.send(`User${req.body.firstName}${req.body.lastName} has signed up succesfully`)
}
else {
	res.send("Please input both LastName & firstName")
};
});
let users=[]

app.put("/change-lastName",(req, res) =>{

	let message;

	for (let i=0; i<users.length; i++){
		if(req.body.firstName == users[i].username){
			users[i].lastName=req.body.lastName;

			message= `user ${req.body.firstName} has succesfully changed the lastName into ${req.body.lastName}`;
			console.log(users);

			break;
		} else {
			message="User does not exist"
		}
		}
	
res.send(message);
});

// tells our server to listen to the port
// if the port is accessed we can run the server
// returns a message that the server is running

app.listen(port,()=> console.log(`Server running at port: ${port}`));



/*==============================================================================================================================================*/

/*ACTIVITY*/

/*7. Export the Postman collection and save it inside the root folder of our application.
8. Create a git repository named S34.
9. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
10. Add the link in Boodle.*/


// 1. Create a GET route that will access the "/home" route that will print out a simple message

app.get("/home",(req,res)=>{

	res.send("Welcome to homepage!");
});


// 2.Process a GET request at the "/home" route using postman.
// 3. Create a GET route that will access the "/users" route that will retrieve all the users in the mock database.

app.get("/users",(req,res)=>{

	res.send(users);
});

// 4. Process a GET request at the "/users" route using postman.

// 5. Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.

app.get("/delete-user",(req,res)=>{
	res.send(`You have deleted the user ${firstName}${lastName} in the database`);
});


